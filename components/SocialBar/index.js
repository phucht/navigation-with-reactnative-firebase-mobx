import React, { Component } from "react";
import { View } from "react-native";
import { RkText, RkButton, RkComponent } from "react-native-ui-kitten";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default class SocialButton extends RkComponent {
  render() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: "flex-start",
          alignItems: "center",
          flexDirection: "row",
          flex: 1
        }}
      >
        <View
          style={{ justifyContent: "center", flexDirection: "row", flex: 1 }}
        >
          <RkButton rkType="clear">
            <Icon name="thumb-up-outline" style={{ color: "#14e7ff" }} />
            <RkText rkType="primary primary4">Like</RkText>
          </RkButton>
        </View>

        <View
          style={{ justifyContent: "center", flexDirection: "row", flex: 1 }}
        >
          <RkButton rkType="clear">
            <Icon name="heart-outline" style={{ color: "#f70025" }} />
            <RkText rkType="primary primary4">Comment</RkText>
          </RkButton>
        </View>

        <View
          style={{ justifyContent: "center", flexDirection: "row", flex: 1 }}
        >
          <RkButton rkType="clear">
            <Icon name="share-variant" style={{ color: "#fff" }} />
            <RkText rkType="primary primary4">Share</RkText>
          </RkButton>
        </View>
      </View>
    );
  }
}
