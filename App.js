import { Asset, AppLoading } from "expo";
import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";
import { StatusBar } from "react-native";
import {
  createRouter,
  NavigationProvider,
  StackNavigation
} from "@expo/ex-navigation";

import LoginScreen from "./screens/LoginScreen";
import RegisterScreen from "./screens/RegisterScreen";
import HomeScreen from "./screens/HomeScreen";
import CreateNewPost from "./screens/CreateNewPost";
import ViewPost from "./screens/ViewPost";
import { Provider } from "mobx-react/native";
import LoginStore from "./Stores/LoginStore";

const stores = { LoginStore };

const assets = [
  require("./assets/beetle.jpg"),
  require("./assets/cat.gif"),
  require("./assets/colorful-windows.jpg"),
  require("./assets/paintbrush.jpg"),
  require("./assets/space.jpg"),
  require("./assets/sparkles.jpg")
];
export const Router = createRouter(() => ({
  home: () => HomeScreen,
  register: () => RegisterScreen,
  login: () => LoginScreen,
  createPost: () => CreateNewPost,
  viewPost: () => ViewPost
}));

export default class App extends React.Component {
  render() {
    return (
      <Provider {...stores}>
        <NavigationProvider router={Router}>
          <StackNavigation initialRoute={Router.getRoute("login")} />
        </NavigationProvider>
      </Provider>
    );
  }
}
