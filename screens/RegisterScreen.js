import React, { Component } from "react";
import { View, Alert } from "react-native";
import {
  RkButton,
  RkTextInput,
  RkAvoidKeyboard,
  RkText
} from "react-native-ui-kitten";

import { Router } from "../App";
import { observer, inject } from "mobx-react/native";
import { observable } from "mobx";

@inject("LoginStore")
@observer
export default class RegisterScreen extends React.Component {
  @observable email = "";
  @observable pass = "";
  @observable phone = "";
  @observable name = "";
  @observable error = "";

  render() {
    console.log(123);
    let textError = null;
    if (this.error.length > 0) {
      textError = <RkText rkType="danger">{this.error}</RkText>;
    }
    return (
      <View
        style={{
          flex: 1,
          paddingHorizontal: 20,
          justifyContent: "center",
          backgroundColor: "#fafafa",
          paddingHorizontal: 10
        }}
      >
        <RkAvoidKeyboard>
          {textError}
          <RkTextInput
            placeholder="Email"
            onChangeText={email => {
              this.email = email;
            }}
          />
          <RkTextInput
            placeholder="Password"
            secureTextEntry
            onChangeText={pass => {
              this.pass = pass;
            }}
          />
          <RkTextInput
            placeholder="Phone"
            onChangeText={phone => {
              this.phone = phone;
            }}
          />
          <RkTextInput
            placeholder="Name"
            onChangeText={name => {
              this.name = name;
            }}
          />
          <RkButton
            style={{ alignItems: "center" }}
            onPress={() => {
              this.props.LoginStore.registerUser(
                this.email,
                this.pass,
                this.phone,
                this.name
              )
                .then(user => {
                  console.log(user);
                  if (user.error) {
                    this.error = user.error;
                  } else {
                    this.props.navigator.push(Router.getRoute("login"));
                  }
                })
                .catch(error => {
                  console.log("error", error);
                });
            }}
          >
            Register
          </RkButton>
        </RkAvoidKeyboard>
      </View>
    );
  }
}
