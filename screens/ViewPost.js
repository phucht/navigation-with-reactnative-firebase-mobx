import React, { Component } from "react";
import { View, ScrollView, Image, ActivityIndicator } from "react-native";
import { RkText, RkCard } from "react-native-ui-kitten";
import { inject, observer } from "mobx-react/native";
import { observable } from "mobx";
import { Router } from "../App";
import SocialButton from "../components/SocialBar";

@inject("LoginStore")
@observer
export default class ViewPost extends React.Component {
  render() {
    console.log("Post", this.props.LoginStore.showPost.url);
    return (
      <ScrollView style={{ paddingVertical: 20 }}>
        <RkCard>
          <Image
            rkCardImg
            source={{ uri: this.props.LoginStore.showPost.url }}
          />
          <View style={{ padding: 5 }}>
            <View>
              <RkText style={{ fontWeight: "800", fontSize: 20 }}>
                {this.props.LoginStore.showPost.title}
              </RkText>
            </View>
            <View>
              <View>
                <RkText>{this.props.LoginStore.showPost.content}</RkText>
              </View>
            </View>
            <View>
              <SocialButton />
            </View>
          </View>
        </RkCard>
      </ScrollView>
    );
  }
}
