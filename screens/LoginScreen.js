import React, { Component } from "react";
import { AppRegistry, Text, View } from "react-native";
import {
  RkButton,
  RkTextInput,
  RkText,
  RkAvoidKeyboard
} from "react-native-ui-kitten";

import { observer, inject } from "mobx-react/native";
import { observable } from "mobx";
import styles from "./screenStyle";
import { Router } from "../App";

@inject("LoginStore")
@observer
export default class LoginScreen extends React.Component {
  @observable email = "zzz@gmail.com";
  @observable password = "123456";
  @observable error = "";

  render() {
    console.log(234);
    return (
      <View style={styles.background}>
        <RkText style={styles.titleLogin}>{"LOGIN".toUpperCase()}</RkText>
        <RkAvoidKeyboard style={{ alignItems: "center" }}>
          <RkTextInput
            style={styles.formLogin}
            rkType="rounded"
            placeholder="Username"
            placeholderTextColor="#fff"
            onChangeText={email => {
              this.email = email;
            }}
          />
          <RkTextInput
            style={styles.formLogin}
            rkType="rounded"
            placeholder="Password"
            placeholderTextColor="#fff"
            secureTextEntry
            onChangeText={password => {
              this.password = password;
            }}
          />
          <RkButton
            rkType="rounded warning"
            style={styles.btnLogin}
            onPress={() => {
              this.props.LoginStore.createNewPost(1, 2);
              this.props.LoginStore.loginUser(this.email, this.password)
                .then(login => {
                  if (login.error) {
                    this.error = login.error;
                  } else {
                    this.props.navigator.push(Router.getRoute("home"));
                  }
                })
                .catch(error => {
                  console.log("error", error);
                });
            }}
          >
            Login
          </RkButton>
        </RkAvoidKeyboard>
      </View>
    );
  }
}
