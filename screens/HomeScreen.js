import React, { Component } from "react";
import {
  View,
  Image,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
  FlatList
} from "react-native";
import { RkText, RkCard, RkTheme, RkButton } from "react-native-ui-kitten";
import { inject, observer } from "mobx-react/native";
import { observable } from "mobx";
import { autobind } from "core-decorators";
import Icon from "react-native-vector-icons/FontAwesome";
import { Router } from "../App";
import SocialButton from "../components/SocialBar";
import styles from "./screenStyle";

@inject("LoginStore")
@observer
export default class HomeScreen extends React.Component {
  @observable title = "";
  @observable content = "";
  @observable url = "";
  @observable posts = [];
  @observable isLoading = true;

  async componentWillMount() {
    let posts = await this.props.LoginStore.getPosts();
    let listPosts = [];
    Object.keys(posts).map(key => {
      let { title, content, url } = posts[key];
      listPosts.push({ key, title, content, url });
    });
    this.posts = listPosts;
    this.isLoading = false;
  }

  _gotoPost = item => {
    this.props.LoginStore.showPost = item;
    this.props.navigator.push(Router.getRoute("viewPost"));
  };

  _renderItem = data => {
    let { item, index } = data;
    return (
      <TouchableOpacity
        activeOpacity={0.5}
        onPress={() => this._gotoPost(item)}
      >
        <RkCard key={item.key}>
          <Image rkCardImg source={{ uri: item.url }} resizeMode="cover" />
          <View rkCardImgOverlay style={{ justifyContent: "flex-end" }}>
            <RkText
              rkType="header2 inverseColor"
              style={{
                fontSize: 15,
                color: "#7bdafc",
                paddingHorizontal: 10
              }}
            >
              {item.title.toUpperCase()}
            </RkText>
            <View rkCardFooter>
              <SocialButton rkType="leftAligned" />
            </View>
          </View>
        </RkCard>
      </TouchableOpacity>
    );
  };
  render() {
    if (this.isLoading) {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            backgroundColor: "#e1e6e8"
          }}
        >
          <ActivityIndicator />
        </View>
      );
    }
    return (
      <View style={{ flex: 1, paddingTop: 25, backgroundColor: "#c6edf2" }}>
        <View style={{ flexDirection: "row", backgroundColor: "#0083db" }}>
          <RkText
            style={{
              backgroundColor: "transparent",
              padding: 10,
              flex: 0.7,
              color: "#fff"
            }}
          >
            Welcome,{this.props.LoginStore.user.email}
          </RkText>
          <View style={{ flex: 0.2 }} />
          <View style={{ flex: 0.1, marginVertical: 5, marginHorizontal: 5 }}>
            <RkButton
              style={{
                backgroundColor: "#fff",
                fontSize: 24,
                width: 35,
                height: 35,
                borderRadius: 30
              }}
              onPress={() => {
                this.props.navigator.push(Router.getRoute("createPost"));
              }}
            >
              <Icon name="plus" />
            </RkButton>
          </View>
        </View>
        {this.posts &&
          this.posts.length > 0 && (
            <FlatList
              initialNumToRender={3}
              data={this.posts}
              extraData={this.props}
              renderItem={this._renderItem}
            />
          )}
        {(!this.posts || this.posts.length === 0) && <View />}
      </View>
    );
  }
}

RkTheme.setType("RkCard", "story", {
  img: {
    height: 100,
    opacity: 0.1
  },
  header: {
    alignSeft: "center",
    textAlign: "center"
  },
  content: {
    alignSeft: "center"
  }
});
