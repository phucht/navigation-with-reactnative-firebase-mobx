import { RkStyleSheet } from "react-native-ui-kitten";

export default RkStyleSheet.create(theme => ({
  //Login
  background: {
    flex: 1,
    paddingHorizontal: 20,
    justifyContent: "center",
    backgroundColor: "#2daaff"
  },
  titleLogin: {
    textAlign: "center",
    color: "#fff",
    fontSize: 30,
    fontWeight: "800"
  },

  formLogin: {
    backgroundColor: "#fff"
  },
  btnLogin: {
    alignItems: "center"
  }
}));
