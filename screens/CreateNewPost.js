import React, { Component } from "react";
import {
  View,
  Image,
  ScrollView,
  ActivityIndicator,
  Modal
} from "react-native";
import { observable } from "mobx";
import { inject, observer } from "mobx-react";
import { RkText, RkButton, RkTextInput } from "react-native-ui-kitten";
import { Router } from "../App";

@inject("LoginStore")
@observer
export default class CreateNewPost extends React.Component {
  @observable uploadUrl = "";
  @observable imgUri = "";
  @observable imageName = "";
  @observable title = "";
  @observable content = "";
  @observable isLoading = false;

  _loadingCreate = async () => {
    let url = await this.props.LoginStore.uploadImage(this.imgUri);
    let post = await this.props.LoginStore.createNewPost(
      this.title,
      this.content,
      url
    );
    this.isLoading = false;
    this.props.navigator.push(Router.getRoute("home"));
  };
  render() {
    if (this.isLoading) {
      return (
        <View style={{ flex: 1, justifyContent: "center" }}>
          <ActivityIndicator />
        </View>
      );
    }
    return (
      <View
        style={{
          flex: 1,
          alignItems: "center",
          justifyContent: "center",
          paddingHorizontal: 15
        }}
      >
        <View style={{ width: 100, height: 100 }}>
          {this.imageName === "" && <View />}
          {this.imageName !== "" && (
            <Image
              resizeMode="contain"
              style={{ backgroundColor: "red", width: 100, height: 100 }}
              source={{ uri: this.imageName }}
            />
          )}
        </View>
        <RkTextInput
          placeholder="Title"
          rkType="success"
          onChangeText={title => {
            this.title = title;
          }}
        />
        <RkTextInput
          placeholder="Content"
          rkType="success"
          multiline={true}
          maxHeight={50}
          style={{ height: 100 }}
          onChangeText={content => {
            this.content = content;
          }}
        />
        <RkButton
          onPress={() => {
            this.isLoading = true;
            this._loadingCreate();
          }}
        >
          Create
        </RkButton>
        <RkButton
          onPress={async () => {
            let result = await this.props.LoginStore._pickImage();
            this.imageName = "data:image/jpeg;base64," + result.base64;
            this.imgUri = result.uri;
          }}
        >
          Submit
        </RkButton>
      </View>
    );
  }
}
