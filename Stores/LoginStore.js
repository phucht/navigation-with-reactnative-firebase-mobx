import { observable, action, computed } from "mobx";
import React from "react";
import { Constants, Google, ImagePicker } from "expo";
import * as firebase from "firebase";
import { Alert } from "react-native";
import { inject, observer } from "mobx-react/native";
import { autobind } from "core-decorators";

var config = {
  apiKey: "AIzaSyBMWgli_Vf0W7WkjZ8pZpO1INkV7gs_yIE",
  authDomain: "personal-69b66.firebaseapp.com",
  databaseURL: "https://personal-69b66.firebaseio.com",
  projectId: "personal-69b66",
  storageBucket: "personal-69b66.appspot.com",
  messagingSenderId: "988421919681"
};

guid = () => {
  s4 = () => {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  };
  return (
    s4() +
    s4() +
    "-" +
    s4() +
    "-" +
    s4() +
    "-" +
    s4() +
    "-" +
    s4() +
    s4() +
    s4()
  );
};

@autobind
class LoginStore {
  @observable firebaseService = null;
  @observable user = {};
  @observable posts = {};
  @observable images = null;
  @observable showPost = {};

  constructor() {
    // Ensure that you do not login twice.
    if (!firebase.apps.length) {
      firebase.initializeApp(config);
    }
  }

  guid = () => {
    s4 = () => {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    };
    return (
      s4() +
      s4() +
      "-" +
      s4() +
      "-" +
      s4() +
      "-" +
      s4() +
      "-" +
      s4() +
      s4() +
      s4()
    );
  };
  registerUser = async (email, password, phone, name) => {
    try {
      let user = await firebase
        .auth()
        .createUserWithEmailAndPassword(email, password);

      return user;
    } catch (error) {
      return { error: error.message };
      console.log(error);
      //Alert.alert("Oops!", `${error}!`);
    }
  };

  loginUser = async (email, password) => {
    try {
      let login = await firebase
        .auth()
        .signInWithEmailAndPassword(email, password);
      this.user = login.user;

      return login;
    } catch (error) {
      console.log(error);
      return { error: error.message };
    }
  };

  createNewPost = async (title, content, url) => {
    let id = this.guid();
    try {
      let create = await firebase
        .database()
        .ref("posts/" + id)
        .set({
          title: title,
          content: content,
          url: url
        });
      return create;
    } catch (error) {
      console.log(error);
    }
  };

  getPosts = async () => {
    try {
      let posts = await firebase
        .database()
        .ref("posts")
        .limitToLast(5)
        .once("value");

      console.log("pots", posts);

      return posts.val();
    } catch (error) {
      console.log(error);
    }
  };

  _pickImage = async () => {
    try {
      let result = await ImagePicker.launchImageLibraryAsync({
        allowsEditing: false,
        base64: true,
        aspect: [4, 3]
      });
      console.log("result:", result);

      if (!result.cancelled) {
        return result;
      }
      return null;
    } catch (error) {
      return null;
      console.log("Errorbase64:", error);
    }
  };

  uploadImage = async url => {
    try {
      const response = await fetch(url);
      const blob = await response.blob();
      const ref = firebase
        .storage()
        .ref()
        .child(guid());
      const snapshot = await ref.put(blob);
      let img = await ref.getDownloadURL();
      return img;
    } catch (error) {
      console.log("uploadError", error);
      return null;
    }
  };
}

const store = new LoginStore();
export default store;
